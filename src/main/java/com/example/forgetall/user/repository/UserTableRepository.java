package com.example.forgetall.user.repository;

import com.example.forgetall.user.domain.user.UserTable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.UUID;

public interface UserTableRepository extends JpaRepository<UserTable, UUID>, JpaSpecificationExecutor<UserTable> {

    @EntityGraph(attributePaths = "phones")
//    @Query("SELECT * FROM  UserTable left join UserPhone")
    List<UserTable> findAll();

//    @EntityGraph(attributePaths = "phones")
    @Query("SELECT u FROM  UserTable u left join fetch u.phones p where u.userId=:userId")
    UserTable getByUserId(@NonNull UUID userId);


//    @EntityGraph(attributePaths = "phones")
//    List<UserTable> findByIdWithPhones();

//    @Query(value = """
//        select p
//        from UserTable p
//        left join fetch p.phones
//        """,
//            countQuery = """
//        select count(p)
//        from UserTable p
//        """
//    )
//    Page<UserTable> findAllWithPhones(Pageable pageable);

    @Query(
            value = """
        select p.userId
        from UserTable p
        """,
            countQuery = """
        select count(p)
        from UserTable p
        """
    )
    Page<UUID> findAllUserId(
            Pageable pageable
    );

    @Query("""
      select p
      from UserTable p
      left join fetch p.phones
      where p.userId in :userIds
    """
    )
    List<UserTable> findAllWithPhones(@Param("userIds") List<UUID> userIds);

    @Modifying(clearAutomatically = true)
    @Query("""
        update UserTable u set u.age = :age where u.userId = :id 
    """)
    void updateAge(@Param("id") UUID id , @Param("age") Integer age);
}
