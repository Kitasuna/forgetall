package com.example.forgetall.user.repository;


import com.example.forgetall.user.domain.organization.Organization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrganizationRepository extends JpaRepository<Organization, UUID> {

}
