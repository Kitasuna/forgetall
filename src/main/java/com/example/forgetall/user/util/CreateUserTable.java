package com.example.forgetall.user.util;


import com.example.forgetall.user.domain.user.Payload;
import com.example.forgetall.user.domain.user.UserPhone;
import com.example.forgetall.user.domain.user.UserTable;
import com.example.forgetall.user.dto.rest.response.UserTableDto;
import com.example.forgetall.user.service.UserTableService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class CreateUserTable {

    private final UserTableService userTableService;

    public UserTable createUser() {
        UserTable userTable = getUserTable();
        UserTable userTableEntity = userTableService.saveUserInDB(userTable);
        return userTableEntity;
    }

    public void createUserList(List<UserTableDto> userTableDtos) {
        // For inbound Rabbit Integration
        List<UserTable> userTables = userTableDtos.stream().map(this::dtoToEntity).toList();
        userTableService.saveUserListInDB(userTables);
    }


    public UserTable createUser(UserTableDto userTableDto) {
        // For inbound Rabbit Integration
        UserTable userTable = dtoToEntity(userTableDto);
        UserTable userTableEntity = userTableService.saveUserInDB(userTable);
        return userTableEntity;
    }

    private UserTable dtoToEntity(UserTableDto userTableDto) {
        UserTable userTable = new UserTable();
        userTable.setPayload(userTableDto.getPayload());
        userTable.setAge(userTableDto.getAge());
        userTable.setBirthDate(userTableDto.getBirthDate());
        userTable.setSimpleDate(userTableDto.getSimpleDate());
        List<UserPhone> userPhones = userTableDto.getPhones()
                .stream().map(f -> new UserPhone(f.getNumber(), userTable)).toList();
        userTable.setPhones(userPhones);
        return userTable;
    }

    private UserTable getUserTable() {
        Payload payload = new Payload("data1",100L, LocalDateTime.now());
        UserTable userTable = new UserTable();
        userTable.setPayload(payload);
        userTable.setAge(10);
        userTable.setBirthDate(ZonedDateTime.now());
        userTable.setSimpleDate(LocalDateTime.now());

        UserPhone userPhone = new UserPhone(123, userTable);
        UserPhone userPhone2 = new UserPhone(345, userTable);
        UserPhone userPhone3 = new UserPhone(543, userTable);

        userTable.setPhones(List.of(userPhone,userPhone2,userPhone3));
        return userTable;
    }


    public UserTableDto getDtoForRabbit() {
        // For outbound RabbitIntegration
        UserTable userTable = getUserTable();
        return UserTableDto.of(userTable);
    }

}
