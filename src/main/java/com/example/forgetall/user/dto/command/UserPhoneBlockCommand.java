package com.example.forgetall.user.dto.command;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class UserPhoneBlockCommand {
    private Integer number;
    private UUID userId;
}
