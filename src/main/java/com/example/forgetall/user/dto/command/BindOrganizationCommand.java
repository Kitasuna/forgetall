package com.example.forgetall.user.dto.command;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@Builder
@Getter
@ToString
public class BindOrganizationCommand {

    @NotNull
    private UUID userId;

    @NotNull
    private UUID orgId;
    
    private Integer version;
}
