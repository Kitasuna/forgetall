package com.example.forgetall.user.dto.rest.request;

import com.example.forgetall.user.validator.PinCodeValidatorConstraint;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.UUID;

@Data
public class UserDto {

    private UUID id;

    @NotNull
    @NotBlank
    private String name;

    @NotNull
    @Min(1)
    @Max(100)
    private Integer age;

    @NotNull
    @PinCodeValidatorConstraint("pin")
    private String pinCode;

    @NotNull
    @Min(0)
    private Integer version;
}
