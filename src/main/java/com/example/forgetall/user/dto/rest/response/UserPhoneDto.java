package com.example.forgetall.user.dto.rest.response;

import com.example.forgetall.user.domain.user.UserPhone;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

@Builder
@Getter
public class UserPhoneDto implements Serializable {

    public static UserPhoneDto of(UserPhone phone) {
        return UserPhoneDto.builder()
                .phoneId(phone.getPhoneId())
                .number(phone.getNumber())
                .isBlocked(phone.getIsBlocked())
                .build();
    }

    private UUID phoneId;

    private Integer number;

    private Boolean isBlocked;

}
