package com.example.forgetall.user.dto.rest.response;


import com.example.forgetall.user.domain.user.Payload;
import com.example.forgetall.user.domain.user.UserTable;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Builder
@Getter
public class UserTableDto implements Serializable {

    public static UserTableDto of(UserTable table) {
       return UserTableDto.builder()
                .userId(table.getUserId())
                .age(table.getAge())
                .birthDate(table.getBirthDate())
                .simpleDate(table.getSimpleDate())
                .payload(table.getPayload())
                .phones(table.getPhones().stream().map(UserPhoneDto::of
                        ).collect(Collectors.toList())
                ).build();
    }

    private UUID userId;

    private Integer age;

    private Payload payload;

    private ZonedDateTime birthDate;

    private LocalDateTime simpleDate;

    private List<UserPhoneDto> phones;

}
