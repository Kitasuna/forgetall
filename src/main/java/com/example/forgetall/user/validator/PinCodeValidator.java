package com.example.forgetall.user.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Objects;

public class PinCodeValidator implements ConstraintValidator<PinCodeValidatorConstraint, String>{

    private String validValue;

    @Override
    public void initialize(PinCodeValidatorConstraint constraintAnnotation) {
        this.validValue = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Objects.nonNull(value) && value.contains(this.validValue);
    }
}
