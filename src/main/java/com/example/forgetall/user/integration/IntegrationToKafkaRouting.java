package com.example.forgetall.user.integration;


import com.example.forgetall.events.UserCreatedEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.integration.kafka.dsl.KafkaProducerMessageHandlerSpec;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class IntegrationToKafkaRouting {



    @Bean(name = "createUserEventChannel")
    public DirectChannel toKafkaFlow() {
        return new DirectChannel();
    }

//    @Bean(name = "createUserEventChannel")
//    public QueueChannel toKafkaFlow() {
//        return new QueueChannel(100);
//    }


    @Bean
    public IntegrationFlow sendToKafkaFlow(ProducerFactory<String, UserCreatedEvent> producerFactory) {
        return IntegrationFlow
                .from(toKafkaFlow())
                .log("sendToKafkaFlow")
                .publishSubscribeChannel(c -> c
                        .subscribe(sf -> sf.handle(
                                kafkaMessageHandler(producerFactory, "topic100")
                                        .timestampExpression("T(Long).valueOf('1487694048633')"),
                                e -> e.id("kafkaProducer1")))
                )
                .get();
    }
//    @Bean
//    public DefaultKafkaHeaderMapper mapper() {
//        return new DefaultKafkaHeaderMapper();
//    }

    private KafkaProducerMessageHandlerSpec<String, UserCreatedEvent, ?> kafkaMessageHandler(
            ProducerFactory<String, UserCreatedEvent> producerFactory, String topic) {
        return Kafka
                .outboundChannelAdapter(producerFactory)
//                .messageKey(m -> m
//                        .getHeaders()
//                        .get(IntegrationMessageHeaderAccessor.SEQUENCE_NUMBER))
//                .headerMapper(mapper())
//                .partitionId(m -> 10)
                .topicExpression("headers[kafka_topic] ?: '" + topic + "'")
                .configureKafkaTemplate(t -> t.id("kafkaTemplate:" + topic));
    }

}
