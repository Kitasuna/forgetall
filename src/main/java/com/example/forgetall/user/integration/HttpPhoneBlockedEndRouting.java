package com.example.forgetall.user.integration;

import com.example.forgetall.events.PhoneBlockedEvent;
import com.example.forgetall.user.eventlistener.PhoneBlockedListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.http.dsl.Http;

@Configuration
public class HttpPhoneBlockedEndRouting {

    @Bean
    public IntegrationFlow httpPhoneBlockedIntEndFlow(PhoneBlockedListener phoneBlockedListener) {
        return IntegrationFlow.from(
                Http.inboundChannelAdapter("/block_phone")
                        .requestMapping(r->r.methods(HttpMethod.PUT))
                        .requestPayloadType(PhoneBlockedEvent.class))
                .handle(r-> phoneBlockedListener.processedEvent((PhoneBlockedEvent) r.getPayload()))
                .get();
    }


}
