package com.example.forgetall.user.integration;

import com.example.forgetall.user.dto.rest.response.UserTableDto;
import com.example.forgetall.user.util.CreateUserTable;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.amqp.dsl.Amqp;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.store.SimpleMessageStore;
import org.springframework.messaging.Message;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

@Configuration
public class RabbitRouting {

    @Bean("rabbitUserChannel")
    public QueueChannel rabbitUserChannel() {
        return new QueueChannel(100);
    }

    @Bean("rabbitErrorChannel")
    public QueueChannel rabbitErrorChannel() {
        return new QueueChannel(100);
    }

    @Bean
    public IntegrationFlow amqpError(AmqpTemplate amqpTemplate) {
        return IntegrationFlow.from(rabbitErrorChannel())
                .handle(Amqp.outboundAdapter(amqpTemplate)
                        .routingKey("queue2"))
                .log("RABBIT Error")
                .get();
    }


    @Bean
    public IntegrationFlow amqpOutbound(AmqpTemplate amqpTemplate) {
        return IntegrationFlow.from(rabbitUserChannel())
//        TODO заменить на chanel + возможные параметры
                .handle(Amqp.outboundAdapter(amqpTemplate)
                            .routingKey("queue1")) // default exchange - route to queue 'queue1'
                .log("RABBIT Outbound")
                .get();
    }

    @Bean
    public IntegrationFlow amqpInbound(ConnectionFactory connectionFactory,
                                       CreateUserTable createUserTable,
                                       TransactionTemplate transactionTemplate
    ) {
        SimpleMessageStore messageGroups = new SimpleMessageStore(30);
        return IntegrationFlow.from( Amqp.inboundAdapter(connectionFactory, "queue1"))
//                        .errorChannel(rabbitErrorChannel()))
            .log("RABBIT Inbound")
                .aggregate(aggregator -> aggregator
                        .correlationStrategy(message-> messageGroups.getMessageCount())
                        .releaseStrategy(group -> group.size() >= 30)
                        .sendPartialResultOnExpiry(true)
                        .expireGroupsUponCompletion(true)
                        .groupTimeout(5000)
                        .messageStore(messageGroups)
                )
                .log()
//                .aggregate(a-> a
//                        .c
//                        .releaseStrategy(g->g.size() > 10)
//                        .messageStore(new SimpleMessageStore(10)))
                .<List<UserTableDto>>handle(listDto->{
                    List<UserTableDto> dtos = (List<UserTableDto>) listDto.getPayload();

                    createUserTable.createUserList(dtos);
//                    transactionTemplate.execute(s->{
//                        dtos.forEach(createUserTable::createUser);
//                       return "ok";
//                    });


                })
            .get();

//        return IntegrationFlow.from( Amqp.inboundGateway(connectionFactory, "queue1")
//                                .configureContainer(s-> {
//                                    s.batchSize(10);
//                                    s.concurrentConsumers(10);
//                                    s.receiveTimeout(2000);
//                                })
//                        .errorChannel(rabbitErrorChannel()))
//
////                .wireTap(sf -> sf.handle(this::error))
//                .log("RABBIT Inbound")
//                .handle(dto->createUserTable.createUser((UserTableDto) dto.getPayload()))
//                .get();
    }

    private void error(Message<?> message) {
       throw new RuntimeException();
    }


    @MessagingGateway(defaultRequestChannel = "rabbitUserChannel", errorChannel = "rabbitErrorChannel")
    public static interface RabbitMQGateway {
        void send(UserTableDto userTableDto);
    }

}
