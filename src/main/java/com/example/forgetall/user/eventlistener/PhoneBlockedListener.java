package com.example.forgetall.user.eventlistener;

import com.example.forgetall.events.PhoneBlockedEvent;
import com.example.forgetall.user.dto.command.UserPhoneBlockCommand;
import com.example.forgetall.user.service.UserTableService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class PhoneBlockedListener {

    private final UserTableService userTableService;

//    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
//    @EventListener
//    @Async
    public void handleEvent(PhoneBlockedEvent event){
        log.warn("PhoneBlockedEvent. Phone number {} is blocked for UserTable entity with ID = {}",event.number(), event.userID());
        processedEvent(event);
    }

    public void processedEvent(@Valid PhoneBlockedEvent event) {
        UserPhoneBlockCommand command = UserPhoneBlockCommand.builder()
                .userId(event.userID())
                .number(event.number())
                .build();
        userTableService.blockPhoneNumber(command);
    }

    @KafkaListener(topics = "topic99", groupId = "group-id1")
    public void kafkaEventHandler(PhoneBlockedEvent event) {
        log.warn("KAFKA. PhoneBlockedEvent. Phone number {} is blocked for UserTable entity with ID = {}",event.number(), event.userID());
        processedEvent(event);
    }
}
