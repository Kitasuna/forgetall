package com.example.forgetall.user.eventlistener;

import com.example.forgetall.events.UserCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.event.EventListener;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@Slf4j
public class UserTableCreatedTransactionalListener {

    private final KafkaTemplate<String, UserCreatedEvent> kafkaTemplate;


    public UserTableCreatedTransactionalListener(
            KafkaTemplate<String, UserCreatedEvent> kafkaTemplate,
            @Qualifier("createUserEventChannel")
    DirectChannel directChannel) {
        this.kafkaTemplate = kafkaTemplate;
        this.directChannel = directChannel;
    }


    private final DirectChannel directChannel;

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @EventListener
    public void handleEventUserCreated(UserCreatedEvent event){
        log.info("UserCreatedEvent. UserTable entity was recorded with ID = {}", event.toString());
        sendMessage(event);
    }


//    private void sendMessage(UserCreatedEvent message) {
//        //    Via Kafka
//        log.info(String.format("Message sent -> %s", message.toString()));
//        kafkaTemplate.send("topic100", message);
//    }

    private void sendMessage(UserCreatedEvent message) {
        //    Via Spring Integration
        // TODO send to direct
        log.info(String.format("Message sent -> %s", message.toString()));
        Message<UserCreatedEvent> build = MessageBuilder.withPayload(message).build();
        directChannel.send(build);
//        kafkaTemplate.send("topic100", message);
    }


}
