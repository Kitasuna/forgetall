package com.example.forgetall.user.aop;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class WriteHelloAspect {

    @Before(value = "@annotation(writeHello)")
    public void writeHello(JoinPoint jp, WriteHello writeHello) {
        log.info("Hello from {} with message = {}", jp.getClass().getName(), writeHello.message());
    }

    @After(value = "@annotation(WriteHello)")
    public void writeGoodbye(JoinPoint jp) {
        log.info("Goodbye from {} ", jp.getClass().getName());
    }

}
