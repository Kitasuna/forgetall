package com.example.forgetall.user.domain.user;

import com.example.forgetall.user.domain.Auditing;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UuidGenerator;

import java.util.UUID;

@Entity
@Table
@NoArgsConstructor
@Setter
@Getter
public class UserPhone extends Auditing {

    public UserPhone(Integer number, UserTable userTable) {
        this.number = number;
        this.userTable = userTable;
    }

    @Id
    @UuidGenerator(style = UuidGenerator.Style.TIME)
    @GeneratedValue
    private UUID phoneId;

    private Integer number;

    private Boolean isBlocked = Boolean.FALSE;

    @ManyToOne
    @JoinColumn(name = "userId")
    private UserTable userTable;



}
