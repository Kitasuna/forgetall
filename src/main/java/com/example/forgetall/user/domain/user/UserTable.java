package com.example.forgetall.user.domain.user;

import com.example.forgetall.user.domain.Auditing;
import com.example.forgetall.user.domain.organization.Organization;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.TimeZoneStorage;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table
@NoArgsConstructor
@Setter
@Getter
public class UserTable extends Auditing {

    @Id
    @UuidGenerator(style = UuidGenerator.Style.TIME)
    @GeneratedValue
    private UUID userId;

    private Integer age;

    @JdbcTypeCode( SqlTypes.JSON )
    private Payload payload;

    @TimeZoneStorage
    private ZonedDateTime birthDate;

    private LocalDateTime simpleDate;

    @OneToMany(targetEntity = UserPhone.class, mappedBy = "userTable", cascade = CascadeType.ALL)
    private List<UserPhone> phones;

    @Version
    private Integer version;

    @ManyToOne(targetEntity = Organization.class)
    private Organization organization;

}
