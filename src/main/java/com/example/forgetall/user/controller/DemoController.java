package com.example.forgetall.user.controller;

import com.example.forgetall.user.domain.user.UserTable;
import com.example.forgetall.user.dto.command.BindOrganizationCommand;
import com.example.forgetall.user.dto.rest.request.UserDto;
import com.example.forgetall.user.dto.rest.response.UserTableDto;
import com.example.forgetall.user.repository.UserTableRepository;
import com.example.forgetall.user.service.BindUserTableToOrganization;
import com.example.forgetall.user.util.CreateUserTable;
import com.example.forgetall.user.service.UserTableService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class DemoController {

    private final UserTableRepository repository;
    private final CreateUserTable service;
    private final BindUserTableToOrganization bindAction;

    private final UserTableService userTableService;

    @PostMapping("/update_age")
    public UserTableDto updateAge(@Valid @RequestBody UserDto user) {
        UserTableDto userTableDto = userTableService.updateAge(user);
        return userTableDto;
    }

    @GetMapping("/get_all_user_with_phones")
    public List<UserTableDto> getAllUserWithPhones(Pageable pageable) {
        return userTableService.getAllUserWithPhones(pageable);
    }

    @PutMapping("/bind_user_to_org")
    public String bindUserToOrg(@Valid @RequestBody BindOrganizationCommand command) {
        UserTable userTable = bindAction.bindOrganization(command);
        return userTable.toString();
    }


    @PostMapping("/add_user")
//    @PreAuthorize("hasAuthority('spring-user')")
    public String addUser(@Valid @RequestBody UserDto user, Principal principal) {
        UserTable user1 = service.createUser();
        return user1.toString();
    }


    @GetMapping("/long_data")
    public  List<UserTableDto> sadas(@RequestParam Integer data) {
//        Payload payload = new Payload("data1",100L);
//        UserTable userTable = new UserTable();
////        userTable.setPayload(payload);
////        userTable.setAge(data);
//
////        HashMap<String, Object> map = new HashMap<>();
////        map.put("age", data);
//
////        Example<UserTable> example = Example.of(userTable);
//
////        Object match = repository.findBy(example,
////                q -> q.project("age",  "payload").as(UserTableDto.class).all()
////        );
//

//        List<UserTable> all = repository.findAll((r,q,cb)->{
//           r.join("phones");
//            Predicate[] predicates = new Predicate[]{};
//            return cb.and(predicates);
//        });

        List<UserTable> all = repository.findAll();

        List<UserTableDto> collect = all.stream().map(UserTableDto::of).collect(Collectors.toList());

        return collect;
    }
}
