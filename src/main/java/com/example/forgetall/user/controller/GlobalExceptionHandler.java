package com.example.forgetall.user.controller;

import jakarta.persistence.OptimisticLockException;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.util.HashMap;
import java.util.NoSuchElementException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({OptimisticLockException.class})
    public ProblemDetail handleCustomException(OptimisticLockException ex, WebRequest request) {

        ProblemDetail pd = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, request.toString());
        pd.setType(URI.create("http://my-app-host.com/errors/not-found"));
        pd.setTitle("Optimistic Lock");
//        pd.setDetail(ex.getMessage());
        pd.setProperty("Details", ex.getMessage());

        return pd;
    }
    @ExceptionHandler({NoSuchElementException.class})
    public ProblemDetail handleCustomException(NoSuchElementException ex, WebRequest request) {

        ProblemDetail pd = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, request.toString());
        pd.setType(URI.create("http://my-app-host.com/errors/not-found"));
        pd.setTitle("Record Not Found");


        return pd;
    }

  @ExceptionHandler({ConstraintViolationException.class})
  public ProblemDetail handleCustomException(ConstraintViolationException ex, WebRequest request) {

    ProblemDetail pd = ProblemDetail.forStatusAndDetail(HttpStatus.UNPROCESSABLE_ENTITY, request.toString());
    pd.setType(URI.create("http://my-app-host.com/errors/not-found"));
    pd.setTitle("Record Not Found");
    pd.setProperty("hostname", "localhost");
    pd.setProperty("msg", ex.getMessage());
    pd.setProperty("cause", ex.getCause().getMessage());
    HashMap<String, String> errors = new HashMap<>();

     ex.getConstraintViolations().forEach(constraint -> {
        String message = constraint.getMessage();
        String propertyPath = constraint.getPropertyPath().toString().split("\\.")[2];
        errors.put(propertyPath, message);
      });
     pd.setProperty("errors", errors);
     return pd;
  }
}