package com.example.forgetall.user.service;


import com.example.forgetall.user.domain.organization.Address;
import com.example.forgetall.user.domain.organization.Organization;
import com.example.forgetall.user.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CreateOrganization {


    private final OrganizationRepository organizationRepository;


    @Transactional
    public Organization createOrganization() {

        Organization organization = new Organization();
        organization.setName("New name");

        Address address = new Address();
        address.setAddress("My address");
//        address.setOrganizations(List.of(organization));

        organization.setAddress(address);

        Organization save = organizationRepository.save(organization);

        return save;
    }

}
