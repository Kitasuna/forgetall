package com.example.forgetall.user.service;

import com.example.forgetall.events.UserCreatedEvent;
import com.example.forgetall.user.aop.WriteHello;
import com.example.forgetall.user.domain.user.UserPhone;
import com.example.forgetall.user.domain.user.UserTable;
import com.example.forgetall.user.dto.command.UserPhoneBlockCommand;
import com.example.forgetall.user.dto.rest.request.UserDto;
import com.example.forgetall.user.dto.rest.response.UserTableDto;
import com.example.forgetall.user.repository.UserTableRepository;
import jakarta.persistence.OptimisticLockException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserTableService {

    private final UserTableRepository userTableRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Transactional
    @WriteHello(message = "My message")
    public UserTableDto updateAge(UserDto userDto) {
        UserTable userTable = userTableRepository.findById(userDto.getId()).orElseThrow();

        if (!userTable.getVersion().equals(userDto.getVersion())) {
            throw new OptimisticLockException("Blabalbal");
        }


        userTable.setAge(userDto.getAge());

        UserTable save = userTableRepository.save(userTable);

//        userTableRepository.updateAge(userDto.getId(), userDto.getAge());

        return UserTableDto.of(save);
    }

    public List<UserTableDto> getAllUserWithPhones(Pageable pageable) {
        Page<UUID> userIds = userTableRepository.findAllUserId(pageable);
        List<UserTable> userTable = userTableRepository.findAllWithPhones(userIds.toList());
//        List<UserTable> userTable = userTableRepository.findAllWithPhones(pageable);

        List<UserTableDto> userTableDtos = userTable.stream().map(UserTableDto::of).toList();
        return userTableDtos;
    }

    @Transactional
    public void saveUserListInDB(List<UserTable> userTable) {
        List<UserTable> userTableEntityList = userTableRepository.saveAll(userTable);
        userTableEntityList.forEach(userTableEntity->{
            List<Integer> phones = userTableEntity.getPhones().stream().map(UserPhone::getNumber).toList();
            UserCreatedEvent userCreatedEvent = new UserCreatedEvent(userTableEntity.getUserId(), phones);
            applicationEventPublisher.publishEvent(userCreatedEvent);
        });
    }

    @Transactional
    public UserTable saveUserInDB(UserTable userTable) {
        UserTable userTableEntity = userTableRepository.save(userTable);
        List<Integer> phones = userTableEntity.getPhones().stream().map(UserPhone::getNumber).toList();
        UserCreatedEvent userCreatedEvent = new UserCreatedEvent(userTableEntity.getUserId(), phones);
        applicationEventPublisher.publishEvent(userCreatedEvent);
        return userTableEntity;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void blockPhoneNumber(UserPhoneBlockCommand command){
        UserTable byUserIdAndPhones_number = userTableRepository.getByUserId(command.getUserId());

        byUserIdAndPhones_number.getPhones().stream()
                .filter(p -> p.getNumber().equals(command.getNumber()))
                .forEach(p->p.setIsBlocked(true));

        userTableRepository.save(byUserIdAndPhones_number);
        System.out.println();
    }

}
