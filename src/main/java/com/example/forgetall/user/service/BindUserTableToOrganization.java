package com.example.forgetall.user.service;


import com.example.forgetall.user.domain.organization.Organization;
import com.example.forgetall.user.domain.user.UserTable;
import com.example.forgetall.user.dto.command.BindOrganizationCommand;
import com.example.forgetall.user.repository.OrganizationRepository;
import com.example.forgetall.user.repository.UserTableRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class BindUserTableToOrganization {

    private final UserTableRepository userTableRepository;
    private final OrganizationRepository organizationRepository;

    @SneakyThrows
    @Transactional
    public UserTable bindOrganization(BindOrganizationCommand command) {
        log.info("Before binding. Command {}", command.toString());
        UserTable userTableRepositoryReferenceById = userTableRepository.findById(command.getUserId()).orElseThrow();
        Organization orgRerenceById = organizationRepository.findById(command.getOrgId()).orElseThrow();
        userTableRepositoryReferenceById.setOrganization(orgRerenceById);
        userTableRepositoryReferenceById.setVersion(command.getVersion());
        log.info("After binding. Command {}", command);
//        Thread.sleep(60000);
        userTableRepository.save(userTableRepositoryReferenceById);
        return userTableRepositoryReferenceById;
    }

}
