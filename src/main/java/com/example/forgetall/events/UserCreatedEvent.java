package com.example.forgetall.events;

import java.util.List;
import java.util.UUID;

public record UserCreatedEvent(UUID userID, List<Integer> number) {
}
