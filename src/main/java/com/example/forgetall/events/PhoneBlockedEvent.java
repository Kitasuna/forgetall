package com.example.forgetall.events;

import java.util.UUID;

public record PhoneBlockedEvent(UUID userID, Integer number) {
}
