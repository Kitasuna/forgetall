package com.example.forgetall.phone.service;

import com.example.forgetall.events.PhoneBlockedEvent;
import com.example.forgetall.phone.dto.command.CheckUserPhoneCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class PhoneBlockService {

    private final ApplicationEventPublisher applicationEventPublisher;

    private final KafkaTemplate<String, PhoneBlockedEvent> kafkaTemplate;

    private final DirectChannel httpChanel;

    public PhoneBlockService(ApplicationEventPublisher applicationEventPublisher,
                             KafkaTemplate<String, PhoneBlockedEvent> kafkaTemplate,
                             @Qualifier("httpPhoneBlockedDirect") DirectChannel httpChanel) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.kafkaTemplate = kafkaTemplate;
        this.httpChanel = httpChanel;
    }

    private void sendEventToKafka(PhoneBlockedEvent event) {
        CompletableFuture<SendResult<String, PhoneBlockedEvent>> send = kafkaTemplate.send("topic99",event);

        send.whenCompleteAsync((r,t)->{
            if (Objects.nonNull(r)) {
                log.info(r.toString());
            }

            if (Objects.nonNull(t)) {
                log.info(t.getMessage());
            }
        });

    }

    private void sendEventToHttp(PhoneBlockedEvent event) {
        Message<PhoneBlockedEvent> message = MessageBuilder.withPayload(event).build();
        httpChanel.send(message);
    }

    public void checkPhone(CheckUserPhoneCommand command){
        if (Math.random()>0.5) {
            PhoneBlockedEvent phoneBlockedEvent = new PhoneBlockedEvent(command.getUserId(), command.getNumber());
//            applicationEventPublisher.publishEvent(phoneBlockedEvent);
//            sendEventToKafka(phoneBlockedEvent);
            sendEventToHttp(phoneBlockedEvent);
        }
    }
}
