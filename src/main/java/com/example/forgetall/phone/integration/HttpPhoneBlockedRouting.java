package com.example.forgetall.phone.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.http.dsl.Http;

@Configuration
public class HttpPhoneBlockedRouting {

    @Bean(name = "httpPhoneBlockedDirect")
    public DirectChannel httpPhoneBlockedDirect() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow httpPhoneBlockedIntFlow() {
        return IntegrationFlow.from(httpPhoneBlockedDirect())
                .handle(Http.outboundChannelAdapter("http://localhost:8095/block_phone")
                        .httpMethod(HttpMethod.PUT))
                .log().get();
    }
}
