package com.example.forgetall.phone.integration;


import com.example.forgetall.events.UserCreatedEvent;
import com.example.forgetall.phone.eventlistener.UserTableCreatedListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.kafka.dsl.Kafka;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConsumerProperties;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Configuration
public class IntegrationFromKafkaRouting {

    @Bean
    public IntegrationFlow readFromKafkaFlow(
            ConsumerFactory<String, UserCreatedEvent> consumerFactory,
            UserTableCreatedListener userTableCreatedListener
    ) {
        ConsumerProperties consumerProperties = new ConsumerProperties("topic100");
        consumerProperties.setGroupId("group-id");
        return IntegrationFlow
                .from(Kafka.inboundChannelAdapter(consumerFactory, consumerProperties),
                        e -> e.poller(Pollers.fixedDelay(Duration.of(1, ChronoUnit.SECONDS))))
                .log("readFromKafkaFlow")
                .handle(message->userTableCreatedListener.handleEventUserCreated((UserCreatedEvent) message.getPayload()))
                .get();
    }

}
