package com.example.forgetall.phone.dto.command;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class CheckUserPhoneCommand {
    private Integer number;
    private UUID userId;
}
