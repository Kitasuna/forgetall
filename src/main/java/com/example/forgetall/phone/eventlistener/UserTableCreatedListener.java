package com.example.forgetall.phone.eventlistener;

import com.example.forgetall.events.UserCreatedEvent;
import com.example.forgetall.phone.dto.command.CheckUserPhoneCommand;
import com.example.forgetall.phone.service.PhoneBlockService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class UserTableCreatedListener {

    private final PhoneBlockService phoneBlockService;

    private void processEvent(UserCreatedEvent event) {
        event.number().forEach(n->
                phoneBlockService.checkPhone(
                        CheckUserPhoneCommand.builder()
                                .userId(event.userID())
                                .number(n)
                                .build()
                )
        );
    }


//    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
//    @EventListener
    public void handleEventUserCreated(UserCreatedEvent event){
        log.info("UserCreatedEvent. UserTable entity was recorded with ID = {}", event.toString());
        processEvent(event);
    }



    // Kafka dircetly
//    @KafkaListener(topics = "topic100",
//            groupId = "group-id")
//    public void consume(UserCreatedEvent event)
//    {
//        log.info(String.format("Message recieved -> %s", event.toString()));
//        processEvent(event);
//    }


    // Kafa via Spring Integration
    public void consume(UserCreatedEvent event)
    {
        log.info(String.format("Message recieved -> %s", event.toString()));
        processEvent(event);
    }
}
