package com.example.forgetall;

import com.example.forgetall.user.dto.rest.response.UserTableDto;
import com.example.forgetall.user.integration.RabbitRouting;
import com.example.forgetall.user.service.BindUserTableToOrganization;
import com.example.forgetall.user.service.CreateOrganization;
import com.example.forgetall.user.util.CreateUserTable;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.config.EnableIntegrationManagement;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.support.TransactionTemplate;

@SpringBootApplication
@EnableAsync
@EnableJpaAuditing
@EnableIntegration
@EnableRabbit
@EnableIntegrationManagement
public class ForgetallApplication {

    public static void main(String[] args) {
        SpringApplication.run(ForgetallApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(
            CreateOrganization createOrganization,
            CreateUserTable createUserTable,
            BindUserTableToOrganization bindUserTableToOrganization,
            TransactionTemplate transactionTemplate,
            RabbitRouting.RabbitMQGateway directChannel
    ){

        return args->{

//            Organization organization = createOrganization.createOrganization();
////            Organization organization2 = createOrganization.createOrganization();
//            UserTable user1 = createUserTable.createUser();
//
//            BindOrganizationCommand command = BindOrganizationCommand.builder()
//                    .userId(user1.getUserId())
//                    .orgId(organization.getId()).build();
//            bindUserTableToOrganization.bindOrganization(command);
//
//            createUserTable.createUser();
//            createUserTable.createUser();


            for (int i = 0; i < 5000; i++) {
                UserTableDto dtoForRabbit = createUserTable.getDtoForRabbit();
//                Message<UserTableDto> message = MessageBuilder.withPayload(dtoForRabbit).build();

                directChannel.send(dtoForRabbit);
            }





//            for (int j = 0; j < 1; j++) {
//                transactionTemplate.execute(e->{
//                    for (int i = 0; i < 10; i++) {
//                        createUserTable.createUser();
//                    }
//                    return "ok";
//                });
//                System.out.println("Transactional completed with i="+j);
//            }
//            System.out.println("Init completed");






        };
    }
}
