package com.example.forgetall.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Principal;
import java.util.Optional;

@Slf4j
public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String name = Optional.ofNullable(authentication).map(Principal::getName).orElse("SYSTEM");
        log.info("Name from authentication = {}", name);
        return Optional.of(name);
//        return Optional.empty();
    }


}
