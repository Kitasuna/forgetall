package com.example.forgetall.config.kafka;

import com.example.forgetall.events.PhoneBlockedEvent;
import com.example.forgetall.events.UserCreatedEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
@EnableKafka
@RequiredArgsConstructor
public class KafkaConfiguration {

    @Value("${kafka.bootstrap-servers}")
    private String bootstrapServers;

    private final ObjectMapper objectMapper;

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
//        props.put(ConsumerConfig.GROUP_ID_CONFIG, "groupId");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
//        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
//        props.put(ConsumerConfig.FETCH_MAX_BYTES_CONFIG, 20 * 1024 * 1024);
        return props;
    }

    @Bean
    public Map<String, Object> producerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
//        props.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, 20 * 1024 * 1024);
        return props;
    }



    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, UserCreatedEvent> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, UserCreatedEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    @Bean
    public ConsumerFactory<String, UserCreatedEvent> consumerFactory() {
        DefaultKafkaConsumerFactory<String, UserCreatedEvent> factory = new DefaultKafkaConsumerFactory<>(consumerConfigs());
        JsonDeserializer<UserCreatedEvent> auditingKafkaJsonDeserializer = new JsonDeserializer<>(UserCreatedEvent.class, objectMapper);
        factory.setValueDeserializer(auditingKafkaJsonDeserializer);
        return factory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, PhoneBlockedEvent> kafkaListenerContainerFactory1() {
        ConcurrentKafkaListenerContainerFactory<String, PhoneBlockedEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory1());
        return factory;
    }

    @Bean
    public ConsumerFactory<String, PhoneBlockedEvent> consumerFactory1() {
        DefaultKafkaConsumerFactory<String, PhoneBlockedEvent> factory = new DefaultKafkaConsumerFactory<>(consumerConfigs());
        JsonDeserializer<PhoneBlockedEvent> auditingKafkaJsonDeserializer = new JsonDeserializer<>(PhoneBlockedEvent.class, objectMapper);
        factory.setValueDeserializer(auditingKafkaJsonDeserializer);
        return factory;
    }

    @Bean
    public ProducerFactory<String, UserCreatedEvent> producerFactory() {
        JsonSerializer<UserCreatedEvent> jsonSerializer = new JsonSerializer<>(objectMapper);
        jsonSerializer.configure(producerConfigs(), false);
        return new DefaultKafkaProducerFactory<>(producerConfigs(), new StringSerializer(), jsonSerializer);
    }

    @Bean
    public KafkaTemplate<String, UserCreatedEvent> kafkaTemplate() {
        KafkaTemplate<String, UserCreatedEvent> template = new KafkaTemplate<>(producerFactory());
        template.setMessageConverter(new StringJsonMessageConverter());
        return template;
    }


    @Bean
    public ProducerFactory<String, PhoneBlockedEvent> producerFactory1() {
        JsonSerializer<PhoneBlockedEvent> jsonSerializer = new JsonSerializer<>(objectMapper);
        jsonSerializer.configure(producerConfigs(), false);
        return new DefaultKafkaProducerFactory<>(producerConfigs(), new StringSerializer(), jsonSerializer);
    }

    @Bean
    public KafkaTemplate<String, PhoneBlockedEvent> kafkaTemplate1() {
        KafkaTemplate<String, PhoneBlockedEvent> template = new KafkaTemplate<>(producerFactory1());
        template.setMessageConverter(new StringJsonMessageConverter());
        return template;
    }

}
