package com.example.forgetall.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuditorAwareConfig {

    @Bean
    public AuditorAwareImpl auditorAware(){
        return new AuditorAwareImpl();
    }
}
